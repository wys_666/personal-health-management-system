package com.marx.healthsys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@TableName("j_menu")
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value = "menu_id", type = IdType.AUTO)
    private Integer menuId;
    private String component;
    private String path;
    private String redirect;
    private String name;
    private String title;
    private String icon;
    private Integer parentId;
    private String isLeaf;
    private Boolean hidden;

    @TableField(exist = false)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<Menu> children;

    @TableField(exist = false)
    private Map<String, Object> meta;

    public Menu() {
    }

    public Menu(Integer menuId, String component, String path, String redirect, String name, String title, String icon, Integer parentId, String isLeaf, Boolean hidden, List<Menu> children, Map<String, Object> meta) {
        this.menuId = menuId;
        this.component = component;
        this.path = path;
        this.redirect = redirect;
        this.name = name;
        this.title = title;
        this.icon = icon;
        this.parentId = parentId;
        this.isLeaf = isLeaf;
        this.hidden = hidden;
        this.children = children;
        this.meta = meta;
    }

    public Map<String, Object> getMeta() {
        meta = new HashMap<>();
        meta.put("title", title);
        meta.put("icon", icon);
        return meta;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "menuId=" + menuId +
                ", component='" + component + '\'' +
                ", path='" + path + '\'' +
                ", redirect='" + redirect + '\'' +
                ", name='" + name + '\'' +
                ", title='" + title + '\'' +
                ", icon='" + icon + '\'' +
                ", parentId=" + parentId +
                ", isLeaf='" + isLeaf + '\'' +
                ", hidden=" + hidden +
                ", children=" + children +
                ", meta=" + meta +
                '}';
    }
}
