package com.marx.healthsys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@TableName("detail")
public class Detail implements Serializable {


    @TableId(type = IdType.AUTO)
    @TableField(value = "id")
    private Integer id;


    @TableField("sport_type")
    private String sportType;

    private String disease;

    private String method;

    private String notes;

    public Detail() {
    }

    public Detail(Integer id, String sportType, String disease, String method, String notes) {
        this.id = id;
        this.sportType = sportType;
        this.disease = disease;
        this.method = method;
        this.notes = notes;
    }

    @Override
    public String toString() {
        return "Detail{" +
                "id=" + id +
                ", sportType='" + sportType + '\'' +
                ", disease='" + disease + '\'' +
                ", method='" + method + '\'' +
                ", notes='" + notes + '\'' +
                '}';
    }
}
