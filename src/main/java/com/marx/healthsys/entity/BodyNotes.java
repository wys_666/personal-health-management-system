package com.marx.healthsys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
@TableName("j_body_notes")
public class BodyNotes {

    private static final long serialVersionUID = 1L;

    @TableField(value = "id")
    private Integer id;

    @TableId(value = "notes_id", type = IdType.AUTO)
    private Integer notesid;

    private String name;

    private Integer age;

    private String gender;

    private Double height;

    private Double weight;
    @TableField(value = "bloodSugar")
    private Double bloodSugar;
    @TableField(value = "bloodPressure")
    private String bloodPressure;
    @TableField(value = "bloodLipid")
    private String bloodLipid;

    @TableField("heart_rate")
    private double heartRate;

    @TableField("vision")
    private Integer vision;

    @TableField("sleep_duration")
    private double sleepDuration;

    @TableField("sleep_quality")
    private String sleepQuality;

    @TableField("smoking")
    private boolean smoking;

    @TableField("drinking")
    private boolean drinking;

    @TableField("exercise")
    private boolean exercise;

    @TableField("food_types")
    private String foodTypes;

    @TableField("water_consumption")
    private double waterConsumption;

    @TableField("Date")
    private Date Date;

    public BodyNotes() {
    }

    public BodyNotes(Integer id, Integer notesid, String name, Integer age, String gender, Double height, Double weight, Double bloodSugar, String bloodPressure, String bloodLipid, double heartRate, Integer vision, double sleepDuration, String sleepQuality, boolean smoking, boolean drinking, boolean exercise, String foodTypes, double waterConsumption, java.util.Date date) {
        this.id = id;
        this.notesid = notesid;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.height = height;
        this.weight = weight;
        this.bloodSugar = bloodSugar;
        this.bloodPressure = bloodPressure;
        this.bloodLipid = bloodLipid;
        this.heartRate = heartRate;
        this.vision = vision;
        this.sleepDuration = sleepDuration;
        this.sleepQuality = sleepQuality;
        this.smoking = smoking;
        this.drinking = drinking;
        this.exercise = exercise;
        this.foodTypes = foodTypes;
        this.waterConsumption = waterConsumption;
        Date = date;
    }

    @Override
    public String toString() {
        return "BodyNotes{" +
                "id=" + id +
                ", notesid=" + notesid +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", height=" + height +
                ", weight=" + weight +
                ", bloodSugar=" + bloodSugar +
                ", bloodPressure='" + bloodPressure + '\'' +
                ", bloodLipid='" + bloodLipid + '\'' +
                ", heartRate=" + heartRate +
                ", vision=" + vision +
                ", sleepDuration=" + sleepDuration +
                ", sleepQuality='" + sleepQuality + '\'' +
                ", smoking=" + smoking +
                ", drinking=" + drinking +
                ", exercise=" + exercise +
                ", foodTypes='" + foodTypes + '\'' +
                ", waterConsumption=" + waterConsumption +
                ", Date=" + Date +
                '}';
    }

}

