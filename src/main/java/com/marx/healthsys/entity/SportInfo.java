package com.marx.healthsys.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@TableName("sport_info")
public class SportInfo implements Serializable {
    @TableId(type = IdType.AUTO)
    @TableField(value = "id")
    private Integer id;

    @TableField(value = "sport_type")
    private String sportType;

    @TableField(value = "suitable_time")
    private String suitableTime;

    @TableField(value = "suitable_heart_rate")
    private String suitableHeartRate;

    @TableField(value = "suitable_frequency")
    private String suitableFrequency;

    @TableField(value = "recommended_speed")
    private String recommendedSpeed;

    public SportInfo() {
    }

    public SportInfo(Integer id, String sportType, String suitableTime, String suitableHeartRate, String suitableFrequency, String recommendedSpeed) {
        this.id = id;
        this.sportType = sportType;
        this.suitableTime = suitableTime;
        this.suitableHeartRate = suitableHeartRate;
        this.suitableFrequency = suitableFrequency;
        this.recommendedSpeed = recommendedSpeed;
    }

    @Override
    public String toString() {
        return "SportInfo{" +
                "id=" + id +
                ", sportType='" + sportType + '\'' +
                ", suitableTime='" + suitableTime + '\'' +
                ", suitableHeartRate='" + suitableHeartRate + '\'' +
                ", suitableFrequency='" + suitableFrequency + '\'' +
                ", recommendedSpeed='" + recommendedSpeed + '\'' +
                '}';
    }

}

