package com.marx.healthsys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;


@Setter
@Getter
@TableName("j_user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String username;
    private String password;
    private String email;
    private String phone;
    private Integer status;
    private String avatar;
    private Integer deleted;

    @TableField(exist = false)
    private List<Integer> roleIdList;

    @TableField(exist = false)
    private String newPassword;

    public User() {
    }

    public User(Integer id, String username, String password, String email, String phone, Integer status, String avatar, Integer deleted, List<Integer> roleIdList, String newPassword) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.phone = phone;
        this.status = status;
        this.avatar = avatar;
        this.deleted = deleted;
        this.roleIdList = roleIdList;
        this.newPassword = newPassword;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", status=" + status +
                ", avatar='" + avatar + '\'' +
                ", deleted=" + deleted +
                ", roleIdList=" + roleIdList +
                ", newPassword='" + newPassword + '\'' +
                '}';
    }

}
