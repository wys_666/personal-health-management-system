package com.marx.healthsys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.marx.healthsys.entity.User;

import java.util.Map;


public interface IUserService extends IService<User> {

    Map<String, Object> login(User user);

    Map<String, Object> getUserInfo(String token);

    void logout(String token);

    boolean addUser(User user);

    User getUserById(Integer id);

    void updateUser(User user);

    void deletUserById(Integer id);

    Map<String, Object> register(User register);


    Map<String, Object> getUserId();

    Map<String, Object> getBodyInfo();

    boolean updateuser(User user);


}