package com.marx.healthsys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.marx.healthsys.entity.Role;


public interface IRoleService extends IService<Role> {

    boolean addRole(Role role);

    Role getRoleById(Integer id);

    void updateRole(Role role);

    void deleteRoleById(Integer id);
}
