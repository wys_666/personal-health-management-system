package com.marx.healthsys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.marx.healthsys.entity.UserRole;


public interface IUserRoleService extends IService<UserRole> {

}
