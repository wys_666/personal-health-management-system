package com.marx.healthsys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.marx.healthsys.entity.UserRole;
import com.marx.healthsys.mapper.UserRoleMapper;
import com.marx.healthsys.service.IUserRoleService;
import org.springframework.stereotype.Service;


@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements IUserRoleService {

}
