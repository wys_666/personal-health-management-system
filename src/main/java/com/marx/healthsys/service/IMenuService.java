package com.marx.healthsys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.marx.healthsys.entity.Menu;

import java.util.List;


public interface IMenuService extends IService<Menu> {

    List<Menu> getAllMenu();

    List<Menu> getMenuListByUserId(Integer userId);
}
