package com.marx.healthsys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.marx.healthsys.entity.Role;


public interface RoleMapper extends BaseMapper<Role> {

}
