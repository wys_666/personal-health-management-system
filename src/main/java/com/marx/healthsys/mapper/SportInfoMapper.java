package com.marx.healthsys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.marx.healthsys.entity.SportInfo;

public interface SportInfoMapper extends BaseMapper<SportInfo> {


}
