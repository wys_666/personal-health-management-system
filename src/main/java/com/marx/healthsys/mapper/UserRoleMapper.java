package com.marx.healthsys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.marx.healthsys.entity.UserRole;

public interface UserRoleMapper extends BaseMapper<UserRole> {

}
