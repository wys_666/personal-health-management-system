package com.marx.healthsys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.marx.healthsys.entity.BodyNotes;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BodyNotesMapper extends BaseMapper<BodyNotes> {

}

