package com.marx.healthsys;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@MapperScan("com.marx.*.mapper")
public class HealthManagerApplication {
    public static void main(String[] args) {
        SpringApplication.run(HealthManagerApplication.class, args);
    }
}
